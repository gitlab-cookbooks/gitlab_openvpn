#!/bin/bash
set -e

add_user() {
    cd /etc/openvpn-ca
    # shellcheck source=/dev/null
    source vars
    echo "*****************************************************************************"
    echo "* $user as CN"
    echo "* $user@gitlab.com as email and leave everything as default."
    echo "*****************************************************************************"
    echo ""

    ./build-key "$user"
    cd /etc/openvpn/client-configs/
    ./make_config.sh "$user"

    cp "files/${user}.ovpn" "/home/$SUDO_USER/."
    chown "$SUDO_USER"  "/home/$SUDO_USER/${user}.ovpn"
    rm "/etc/openvpn/client-configs/files/${user}.ovpn"
    rm "/etc/openvpn-ca/keys/${user}.key"
    if ! id -u "$user"; then
        echo "User does not exist on the vpn server, please add a new account on the vpn role for $user"
        exit 1
    fi
    sudo -Hu "$user" bash -c "cd /home/$user; google-authenticator -t -d -w 3 -r 3 -R 30 --label='GitLab OpenVPN'"
    echo "*****************************************************************************"
    echo "* Copy /home/$SUDO_USER/${user}.ovpn to your computer and delete the file"
    echo "* Upload ${user}.ovpn and a screenshot of the above codes/QR to,"
    echo "*   a google drive folder and give access to the invidual to download."
    echo "*****************************************************************************"
}

remove_user() {
    rm "/home/$user/.google_authenticator"
    rm "/etc/openvpn-ca/keys/$user.crt"
    rm "/etc/openvpn-ca/keys/$user.csr"
}

usage() {
    echo "$0 <add|remove> <username>"
    exit 1
}

[[ -z $2 ]] && usage
action=$1
user=$2

if [[ $EUID -ne 0 ]]; then
    echo "This script must be run as root" 
    exit 1
fi

if [[ -z $SUDO_USER  ]]; then
    echo "SUDO_USER not defined, must be set to originating user"
    exit 1
fi

case $action in
    add)
        if [[ -f "/home/$user/.google_authenticator" ]]; then
            echo "User $user already has vpn access, please remove first"
            exit 1
        fi
        echo "* Adding $user"
        add_user
        ;;
    remove)    
        if [[ ! -f "/home/$user/.google_authenticator" ]]; then
            echo "User $user doesn't appear to have vpn access, not doing anything"
            exit 1
        fi
        echo "* Removing $user"
        remove_user
        ;;
    *)
        echo "Invalid action: $action"
        usage
esac
