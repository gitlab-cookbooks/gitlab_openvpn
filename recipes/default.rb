include_recipe 'chef-vault'
openvpn_secrets = chef_vault_item(node['gitlab_openvpn']['chef_vault'],node['gitlab_openvpn']['chef_vault_item'])['gitlab_openvpn']

package 'openvpn'
package 'libpam-google-authenticator'
package 'libqrencode3'

file '/etc/sysctl.cnf' do
  owner 'root'
  group 'root'
  mode '644'
  action :create_if_missing
end

replace_or_add 'net.ipv4.ip_forward' do
  path '/etc/sysctl.cnf'
  pattern 'net.ipv4.ip_forward=*'
  line "net.ipv4.ip_forward=1"

  notifies :restart, 'service[openvpn]', :delayed
end

execute "enable forwarding" do
  command "sysctl -w net.ipv4.ip_forward=1"
  not_if "grep 1 /proc/sys/net/ipv4/ip_forward"
end

directory "#{node['gitlab_openvpn']['conf_dir']}" do
  owner 'root'
  group 'root'
  mode '755'
end

# CA Certificate
file "#{node['gitlab_openvpn']['conf_dir']}/ca.crt" do
  content "#{openvpn_secrets['ssl']['ca_certificate']}"
  owner 'root'
  group 'root'
  mode '644'

  notifies :restart, 'service[openvpn]', :delayed
end

# CA Key
file "#{node['gitlab_openvpn']['conf_dir']}/ca.key" do
  content "#{openvpn_secrets['ssl']['ca_key']}"
  owner 'root'
  group 'root'
  mode '600'

  notifies :restart, 'service[openvpn]', :delayed
end

# DH2048.pem
file "#{node['gitlab_openvpn']['conf_dir']}/dh2048.pem" do
  content "#{openvpn_secrets['ssl']['dh2048']}"
  owner 'root'
  group 'root'
  mode '644'

  notifies :restart, 'service[openvpn]', :delayed
end

# Server Certificate
file "#{node['gitlab_openvpn']['conf_dir']}/#{node['gitlab_openvpn']['key_name']}.crt" do
  content "#{openvpn_secrets['ssl']['server_certificate']}"
  owner 'root'
  group 'root'
  mode '644'

  notifies :restart, 'service[openvpn]', :delayed
end

# Server Key
file "#{node['gitlab_openvpn']['conf_dir']}/#{node['gitlab_openvpn']['key_name']}.key" do
  content "#{openvpn_secrets['ssl']['server_key']}"
  owner 'root'
  group 'root'
  mode '600'

  notifies :restart, 'service[openvpn]', :delayed
end

# TA Key
file "#{node['gitlab_openvpn']['conf_dir']}/ta.key" do
  content "#{openvpn_secrets['ssl']['ta_key']}"
  owner 'root'
  group 'root'
  mode '600'

  notifies :restart, 'service[openvpn]', :delayed
end

# Certificate Revocation List
file "#{node['gitlab_openvpn']['conf_dir']}/crl.pem" do
  content "#{openvpn_secrets['ssl']['crl']}"
  owner 'root'
  group 'root'
  mode '644'

  notifies :restart, 'service[openvpn]', :delayed
end

cookbook_file '/etc/pam.d/openvpn' do
  source 'pam.openvpn'
  owner 'root'
  group 'root'
  mode '644'
end

cookbook_file '/usr/local/bin/vpn-setup' do
  source 'vpn-setup'
  owner 'root'
  group 'root'
  mode '0755'
end

template "#{node['gitlab_openvpn']['conf_dir']}/server.conf" do
  source 'server.conf.erb'
  owner 'root'
  group 'root'
  mode '644'
  variables(openvpn_secrets.to_hash())
  notifies :restart, 'service[openvpn]'
end

execute 'allow VPN pool out' do
  command "/sbin/iptables -I FORWARD -i tun0 -o eth0 -s #{node['gitlab_openvpn']['client_address_pool_slash_format']} -m conntrack --ctstate NEW -j ACCEPT"
  not_if "/sbin/iptables -C FORWARD -i tun0 -o eth0 -s #{node['gitlab_openvpn']['client_address_pool_slash_format']} -m conntrack --ctstate NEW -j ACCEPT"
end

execute 'allow established connections' do
  command "/sbin/iptables -I FORWARD -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT"
  not_if "/sbin/iptables -C FORWARD -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT"
end

execute 'NAT VPN clients' do
  command "/sbin/iptables -t nat -I POSTROUTING -o eth0 -s #{node['gitlab_openvpn']['client_address_pool_slash_format']} -j MASQUERADE"
  not_if "/sbin/iptables -C POSTROUTING -t nat -o eth0 -s #{node['gitlab_openvpn']['client_address_pool_slash_format']} -j MASQUERADE"
end

service 'openvpn' do
  supports :status => true, :restart => true
  action [ :enable, :start ]
end
