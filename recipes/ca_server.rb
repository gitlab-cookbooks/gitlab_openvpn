include_recipe 'chef-vault'
openvpn_secrets = chef_vault_item(node['gitlab_openvpn']['chef_vault'],node['gitlab_openvpn']['chef_vault_item'])['gitlab_openvpn']

package 'easy-rsa'
package 'libqrencode3'

directory "#{node['gitlab_openvpn']['ca_dir']}" do
  owner 'root'
  group 'root'
  mode '755'
end

directory "#{node['gitlab_openvpn']['ca_dir']}/keys" do
  owner 'root'
  group 'root'
  mode '700'
end

directory "#{node['gitlab_openvpn']['conf_dir']}" do
  owner 'root'
  group 'root'
  mode '755'
end

directory "#{node['gitlab_openvpn']['conf_dir']}/client-configs" do
  owner 'root'
  group 'root'
  mode '755'
end

directory "#{node['gitlab_openvpn']['conf_dir']}/client-configs/files" do
  owner 'root'
  group 'root'
  mode '700'
end

# CA Certificate in CA directory
file "#{node['gitlab_openvpn']['ca_dir']}/keys/ca.crt" do
  content "#{openvpn_secrets['ssl']['ca_certificate']}"
  owner 'root'
  group 'root'
  mode '644'
end

# CA Key in CA directory
file "#{node['gitlab_openvpn']['ca_dir']}/keys/ca.key" do
  content "#{openvpn_secrets['ssl']['ca_key']}"
  owner 'root'
  group 'root'
  mode '600'
end

# DH2048 in CA directory
file "#{node['gitlab_openvpn']['ca_dir']}/keys/dh2048.pem" do
  content "#{openvpn_secrets['ssl']['dh2048']}"
  owner 'root'
  group 'root'
  mode '644'
end

# Server Certificate in CA directory
file "#{node['gitlab_openvpn']['ca_dir']}/keys/#{node['gitlab_openvpn']['key_name']}.crt" do
  content "#{openvpn_secrets['ssl']['server_certificate']}"
  owner 'root'
  group 'root'
  mode '644'
end

# Server Key in CA directory
file "#{node['gitlab_openvpn']['ca_dir']}/keys/#{node['gitlab_openvpn']['key_name']}.key" do
  content "#{openvpn_secrets['ssl']['server_key']}"
  owner 'root'
  group 'root'
  mode '600'
end

# TA Key in CA directory
file "#{node['gitlab_openvpn']['ca_dir']}/keys/ta.key" do
  content "#{openvpn_secrets['ssl']['ta_key']}"
  owner 'root'
  group 'root'
  mode '600'
end

# CRL in CA directory
file "#{node['gitlab_openvpn']['ca_dir']}/keys/crl.pem" do
  content "#{openvpn_secrets['ssl']['crl']}"
  owner 'root'
  group 'root'
  mode '644'
end

template "#{node['gitlab_openvpn']['ca_dir']}/vars" do
  source 'vars.erb'
  owner 'root'
  group 'root'
  mode '644'
end

template "#{node['gitlab_openvpn']['conf_dir']}/client-configs/base.conf" do
  source 'base.conf.erb'
  owner 'root'
  group 'root'
  mode '644'
end

template "#{node['gitlab_openvpn']['conf_dir']}/client-configs/make_config.sh" do
  source 'make_config.sh.erb'
  owner 'root'
  group 'root'
  mode '700'
end
