# gitlab_openvpn

This cookbook will install and configure an OpenVPN Server along with
Time-based One-time Password (TOTP) via the Google Authenticator library.

There are two recipes. The default installs an OpenVPN server using the certificates
in the Chef databags and requires all users to have a `~/.google_authenticator` file
containing their TOTP secret.

The second, `ca_server` installs all the files needed to create new certificates and
should only be installed on a single server.

Client certificates and TOTP secrets must be manually created on the server acting
as a Certificate Authority.

## I need access to the VPN, how can I get it?

Ok, if you are allowed to get production access you should already have knife and
ssh access to production hosts, these are the tools you will need to gain access.

If you are not allowed to ssh into production or don't have knife access, but
you still need access to the VPN for any other reason, please cut an issue in
the infrastructure issue tracker and label it as ~"access request"

To reach the whole fleet you will need to follow the following instructions step by step

### Request VPN Access
Check these [instructions](https://gitlab.com/gitlab-com/runbooks/blob/master/howto/vpn-access.md#instructions-for-users-requesting-access) on how to request and access the VPN

### Create a client certificate
1. Find the VPN server with the Certificate Authority installed (ask in #production if you don't have Chef access)
```
knife search roles:vpn-base -a FQDN
```

1. SSH to the VPN server

1. Become root `sudo bash`

1. Generate your configuration or a configuration for a different user by running this commands:

```
/usr/local/bin/vpn-setup add <username>
```

**Note**: If generating a client certificate for a user other than your own
ensure that the chef user cookbook is configured to create the account for the
`vpn-base` role.

Make sure that `CN` is your username, and the email is correct.

When completed you will see the QR code and recovery codes.

The verification code is the first password that you can use to login to the
VPN, but this code may expire. You should use the 6-digit password provided in
the Google Authenticator app.

Do NOT confuse the secret key with the 6-digit password. The secret key does
not work as a password.

The emergency scratch codes are only necessary if you need to login to the VPN
but don't have access to your mobile phone. If you decide to print them out,
keep them in a safe location. The are also saved in ~/.google_authenticator.

If using more than one OpenVPN server the resulting `~/.google_authenticator` file
will need to be copied to your home directory on each server.

### How to configure Tunnelblick

Copy the ovpn file to your client. Then open Tunnelblick and drag-drop the ovpn file on it.

If you see a pop-up window asking to use the down-root plugin for OpenVPN click on "Always use the plugin".

Depending on your network connection, it may be necessary to disable setting a nameserver.
Go to `Settings` tab of the configuration and change `Set nameserver` to `Do not set nameserver` in `Set DNS/WINS`.
Otherwise you may not be able to resolve hostnames while connected to VPN.

Then try to connect. When prompted for authentication enter the OS username as the user and the TOTP as the password.

Enjoy and be responsible!


## For administrators

### Creating a new certificate authority

To create a new certificate authority and wipe out any existing certificates, run
the following as root on the certificate server:

```
cd /etc/openvpn-ca
source vars
./clean-all
./build-ca
./build-key-server vpn.gitlab.net
./build-dh
openvpn --genkey --secret keys/ta.key
```

### How to generate a TOTP secret for another user

As root you can create a TOTP secret file for any user:

```
google-authenticator -t -d --label="GitLab OpenVPN" -s ~<username>/.google_authenticator -w 3 -r 3 -R 30 -Q UTF8
chown <username>.<username> ~<username>/.google_authenticator
```

The secret token will be displayed on the screen in URL, QR Code, and numeric
formats. Any of these can be sent to the user via a secure means of communication.
The user's `.google_authenticator` file will then need to be manually copied to
each VPN server.

### Revoking a client certificate

To revoke and invalidate a client certificate, run the following commands on
the certificate server:

```
cd /etc/openvpn-ca
source vars
sudo ./revoke-full <username>
```

The contents of the `/etc/openvpn-ca/keys/crl.pem` file then need to be added to
the Chef databag `['gitlab_openvpn']['ssl']['crl']`, which will result in the
file being copied to all OpenVPN servers.

